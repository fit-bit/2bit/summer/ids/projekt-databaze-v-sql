-- Vytvoření databáze knihovny a inicializace tabulek zkušebnímy daty
-- Autoři: Matěj Kudera (xkuder04), Rostislav Horák (xhorak71)

set serveroutput on;

-- Odmazání tabulek při jejich existenci --
DROP TABLE kontaktni_udaje CASCADE CONSTRAINTS;
DROP TABLE mesto CASCADE CONSTRAINTS;
DROP TABLE nakladatelstvi CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_upominka_kniha CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_upominka_casopis CASCADE CONSTRAINTS;
DROP TABLE ctenar_upominka_kniha CASCADE CONSTRAINTS;
DROP TABLE ctenar_upominka_casopis CASCADE CONSTRAINTS;
DROP TABLE zamestnanec CASCADE CONSTRAINTS;
DROP TABLE pozice CASCADE CONSTRAINTS;
DROP TABLE ctenar CASCADE CONSTRAINTS;
DROP TABLE autor CASCADE CONSTRAINTS;
DROP TABLE kniha CASCADE CONSTRAINTS;
DROP TABLE casopis CASCADE CONSTRAINTS;
DROP TABLE autor_napsal_kniha CASCADE CONSTRAINTS;
DROP TABLE autor_napsal_casopis CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_rezervace_kniha CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_rezervace_casopis CASCADE CONSTRAINTS;
DROP TABLE ctenar_rezervace_kniha CASCADE CONSTRAINTS;
DROP TABLE ctenar_rezervace_casopis CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_vypujcka_kniha CASCADE CONSTRAINTS;
DROP TABLE zamestnanec_vypujcka_casopis CASCADE CONSTRAINTS;
DROP TABLE ctenar_vypujcka_kniha CASCADE CONSTRAINTS;
DROP TABLE ctenar_vypujcka_casopis CASCADE CONSTRAINTS;

-- Tvorba tabulek --
CREATE TABLE kontaktni_udaje (
    kontaktni_udaje_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    kontaktni_udaje_cp NUMBER(5) NOT NULL,
    kontaktni_udaje_ulice VARCHAR2(255) NOT NULL,
    mesto_ID INTEGER NOT NULL,
    kontaktni_udaje_tel NUMBER(12),
    kontaktni_udaje_email VARCHAR2(255) NOT NULL,
    CONSTRAINT kontaktni_udaje_email_constraint CHECK(kontaktni_udaje_email LIKE '%___@___%'), -- Správný formát emailu
    CONSTRAINT kontaktni_udaje_cp_constraint CHECK(kontaktni_udaje_cp > 0), -- Kontrtola nezáporných čísel
    CONSTRAINT kontaktni_udaje_tel_constraint CHECK(kontaktni_udaje_tel > 0)
);

CREATE TABLE mesto (
    mesto_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    mesto_nazev VARCHAR2(255) NOT NULL,
    mesto_psc NUMBER(5) NOT NULL,
    mesto_stat VARCHAR2(255),
    CONSTRAINT mesto_psc_constraint CHECK(mesto_psc > 0) -- Kontrtola nezáporných čísel
);

CREATE TABLE nakladatelstvi (
    nakladatelstvi_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    nakladatelstvi_nazev VARCHAR2(255) NOT NULL,
    kontaktni_udaje_ID INTEGER
);

CREATE TABLE zamestnanec_upominka_kniha (
    upominka_ID INTEGER GENERATED ALWAYS AS IDENTITY,
    upominka_vystaveno DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE zamestnanec_upominka_casopis (
    upominka_ID INTEGER GENERATED ALWAYS AS IDENTITY,
    upominka_vystaveno DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE ctenar_upominka_kniha (
    upominka_ID INTEGER GENERATED ALWAYS AS IDENTITY,
    upominka_vystaveno DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE ctenar_upominka_casopis (
    upominka_ID INTEGER GENERATED ALWAYS AS IDENTITY,
    upominka_vystaveno DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE zamestnanec (
    zamestnanec_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    zamestnanec_jmeno VARCHAR2(255) NOT NULL,
    zamestnanec_prijmeni VARCHAR2(255) NOT NULL,
    pozice_ID INTEGER NOT NULL,
    zamestnanec_rodne_cislo NUMBER(10) NOT NULL,
    kontaktni_udaje_ID INTEGER NOT NULL,
    CONSTRAINT zamestnanec_rodne_cislo_constraint CHECK((zamestnanec_rodne_cislo > 0) and (MOD(zamestnanec_rodne_cislo, 11) = 0))-- Kontrola rodného čísla
);

CREATE TABLE pozice (
    pozice_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    pozice_nazev VARCHAR2(255) NOT NULL,
    pozice_plat NUMBER(5) NOT NULL,
    CONSTRAINT pozice_plat_constraint CHECK(pozice_plat > 0) -- Nezáporné hodnoty
);

CREATE TABLE ctenar (
   ctenar_ID INTEGER PRIMARY KEY,
   ctenar_jmeno VARCHAR2(255) NOT NULL,
   ctenar_prijmeni VARCHAR2(255) NOT NULL,
   ctenar_clen_od DATE NOT NULL,
   ctenar_clen_do DATE NOT NULL,
   kontaktni_udaje_ID INTEGER NOT NULL
); 

CREATE TABLE autor (
    autor_ID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    autor_jmeno VARCHAR2(255) NOT NULL,
    autor_prijmeni VARCHAR2(255) NOT NULL,
    autor_zanr VARCHAR2(255),
    autor_obdobi VARCHAR2(255)
); 

CREATE TABLE kniha (
    kniha_ISBN CHAR(10) PRIMARY KEY,
    kniha_jmeno VARCHAR2(255) NOT NULL,
    kniha_pocet_stran NUMBER(5),
    kniha_pocet_vytisku NUMBER(2) NOT NULL,
    kniha_rok_vydani NUMBER(4) NOT NULL,
    kniha_zanr VARCHAR2(255),
    kniha_cislo_vydani NUMBER(2),
    nakladatelstvi_ID INTEGER NOT NULL,
    CONSTRAINT kniha_pocet_stran_constraint CHECK(kniha_pocet_stran > 0), -- Nezáporné hodnoty
    CONSTRAINT kniha_pocet_vytisku_constraint CHECK(kniha_pocet_vytisku > 0),
    CONSTRAINT kniha_rok_vydani_constraint CHECK(kniha_rok_vydani > 0),
    CONSTRAINT kniha_cislo_vydani_constraint CHECK(kniha_cislo_vydani > 0),
    CONSTRAINT ISBN_chars_constraint CHECK(kniha_ISBN NOT LIKE '%[^0-9]%'), -- Správný formát ISBN
    CONSTRAINT ISBN_constraint CHECK( MOD(10 * SUBSTR (kniha_ISBN, 1, 1)
                                    + 9 * SUBSTR (kniha_ISBN, 2, 1)
                                    + 8 * SUBSTR (kniha_ISBN, 3, 1)
                                    + 7 * SUBSTR (kniha_ISBN, 4, 1)
                                    + 6 * SUBSTR (kniha_ISBN, 5, 1)
                                    + 5 * SUBSTR (kniha_ISBN, 6, 1)
                                    + 4 * SUBSTR (kniha_ISBN, 7, 1)
                                    + 3 * SUBSTR (kniha_ISBN, 8, 1)
                                    + 2 * SUBSTR (kniha_ISBN, 9, 1)
                                   + SUBSTR (kniha_ISBN, 10, 1)
                                    , 11) = 0)
); 

CREATE TABLE casopis (
    casopis_ISSN CHAR(10),
    casopis_jmeno VARCHAR2(255) NOT NULL,
    casopis_pocet_stran NUMBER(5),
    casopis_pocet_vytisku NUMBER(2) NOT NULL,
    casopis_rok_vydani NUMBER(4) NOT NULL,
    casopis_cislo NUMBER(5),
    casopis_rocnik NUMBER(4),
    casopis_tema VARCHAR2(255),
    nakladatelstvi_ID INTEGER NOT NULL,
    CONSTRAINT casopis_pocet_stran_constraint CHECK(casopis_pocet_stran > 0), -- Nezáporné hodnoty
    CONSTRAINT casopis_pocet_vytisku_constraint CHECK(casopis_pocet_vytisku > 0),
    CONSTRAINT casopis_rok_vydani_constraint CHECK(casopis_rok_vydani > 0),
    CONSTRAINT casopis_cislo_constraint CHECK(casopis_cislo > 0),
    CONSTRAINT casopis_rocnik_constraint CHECK(casopis_rocnik > 0),
    CONSTRAINT ISSN_chars_constraint CHECK(casopis_ISSN NOT LIKE '%[^0-9]%'), -- Správný formát ISSN
    CONSTRAINT ISSN_constraint CHECK( MOD( 10 * SUBSTR (casopis_ISSN, 1, 1)
                                    + 9 * SUBSTR (casopis_ISSN, 2, 1)
                                    + 8 * SUBSTR (casopis_ISSN, 3, 1)
                                    + 7 * SUBSTR (casopis_ISSN, 4, 1)
                                    + 6 * SUBSTR (casopis_ISSN, 5, 1)
                                    + 5 * SUBSTR (casopis_ISSN, 6, 1)
                                    + 4 * SUBSTR (casopis_ISSN, 7, 1)
                                    + 3 * SUBSTR (casopis_ISSN, 8, 1)
                                    + 2 * SUBSTR (casopis_ISSN, 9, 1)
                                    + SUBSTR (casopis_ISSN, 10, 1)
                                    ,11) = 0)
);

-- Tabulky pro vstah M:N --
CREATE TABLE autor_napsal_kniha (
    autor_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE autor_napsal_casopis (
    autor_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE zamestnanec_rezervace_kniha (
    rezervace_do_kdy DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE ctenar_rezervace_kniha (
    rezervace_do_kdy DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE zamestnanec_rezervace_casopis (
    rezervace_do_kdy DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE ctenar_rezervace_casopis (
    rezervace_do_kdy DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE zamestnanec_vypujcka_kniha (
    vypujcka_od_kdy DATE NOT NULL,
    vypujcka_do_kdy DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE zamestnanec_vypujcka_casopis (
    vypujcka_od_kdy DATE NOT NULL,
    vypujcka_do_kdy DATE NOT NULL,
    zamestnanec_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

CREATE TABLE ctenar_vypujcka_kniha (
    vypujcka_od_kdy DATE NOT NULL,
    vypujcka_do_kdy DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    kniha_ISBN CHAR(10) NOT NULL
);

CREATE TABLE ctenar_vypujcka_casopis (
    vypujcka_od_kdy DATE NOT NULL,
    vypujcka_do_kdy DATE NOT NULL,
    ctenar_ID INTEGER NOT NULL,
    casopis_ISSN  CHAR(10) NOT NULL,
    casopis_cislo NUMBER(5) NOT NULL
);

-- Nastavení klíčů --
-- PK --
ALTER TABLE casopis ADD CONSTRAINT PK_casopis PRIMARY KEY (casopis_ISSN, casopis_cislo);

ALTER TABLE zamestnanec_upominka_kniha ADD CONSTRAINT PK_zamestnanec_upominka_kniha PRIMARY KEY (zamestnanec_ID, kniha_ISBN);
ALTER TABLE zamestnanec_upominka_casopis ADD CONSTRAINT PK_zamestnanec_upominka_casopis PRIMARY KEY (zamestnanec_ID, casopis_ISSN, casopis_cislo);
ALTER TABLE ctenar_upominka_kniha ADD CONSTRAINT PK_ctenar_upominka_kniha PRIMARY KEY (ctenar_ID, kniha_ISBN);
ALTER TABLE ctenar_upominka_casopis ADD CONSTRAINT PK_ctenar_upominka_casopis PRIMARY KEY (ctenar_ID, casopis_ISSN, casopis_cislo);

ALTER TABLE autor_napsal_kniha ADD CONSTRAINT PK_autor_napsal_kniha PRIMARY KEY (autor_ID, kniha_ISBN);
ALTER TABLE autor_napsal_casopis ADD CONSTRAINT PK_autor_napsal_casopis PRIMARY KEY (autor_ID, casopis_ISSN, casopis_cislo);
ALTER TABLE zamestnanec_rezervace_kniha ADD CONSTRAINT PK_zamestnanec_rezervace_kniha PRIMARY KEY (zamestnanec_ID, kniha_ISBN);
ALTER TABLE ctenar_rezervace_kniha ADD CONSTRAINT PK_ctenar_rezervace_kniha PRIMARY KEY (ctenar_ID, kniha_ISBN);
ALTER TABLE zamestnanec_rezervace_casopis ADD CONSTRAINT PK_zamestnanec_rezervace_casopis PRIMARY KEY (zamestnanec_ID, casopis_ISSN, casopis_cislo);
ALTER TABLE ctenar_rezervace_casopis ADD CONSTRAINT PK_ctenar_rezervace_casopis PRIMARY KEY (ctenar_ID, casopis_ISSN, casopis_cislo);
ALTER TABLE zamestnanec_vypujcka_kniha ADD CONSTRAINT PK_zamestnanec_vypujcka_kniha PRIMARY KEY (zamestnanec_ID, kniha_ISBN);
ALTER TABLE zamestnanec_vypujcka_casopis ADD CONSTRAINT PK_zamestnanec_vypujcka_casopis PRIMARY KEY (zamestnanec_ID, casopis_ISSN, casopis_cislo);
ALTER TABLE ctenar_vypujcka_kniha ADD CONSTRAINT PK_ctenar_vypujcka_kniha PRIMARY KEY (ctenar_ID, kniha_ISBN);
ALTER TABLE ctenar_vypujcka_casopis ADD CONSTRAINT PK_ctenar_vypujcka_casopis PRIMARY KEY (ctenar_ID, casopis_ISSN, casopis_cislo);

-- FK --
ALTER TABLE nakladatelstvi ADD CONSTRAINT FK_nakladatelstvi__kontaktni_udaje_ID FOREIGN KEY (kontaktni_udaje_ID) REFERENCES kontaktni_udaje ON DELETE CASCADE;
ALTER TABLE zamestnanec ADD CONSTRAINT FK_zamestnanec__pozice_ID FOREIGN KEY (pozice_ID) REFERENCES pozice ON DELETE CASCADE;
ALTER TABLE kontaktni_udaje ADD CONSTRAINT FK_kontaktni_udaje__mesto_ID FOREIGN KEY (mesto_ID) REFERENCES mesto ON DELETE CASCADE;

ALTER TABLE zamestnanec_upominka_kniha ADD CONSTRAINT FK_zamestnanec_upominka_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE zamestnanec_upominka_kniha ADD CONSTRAINT FK_zamestnanec_upominka_kniha__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE zamestnanec_upominka_casopis ADD CONSTRAINT FK_zamestnanec_upominka_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;
ALTER TABLE zamestnanec_upominka_casopis ADD CONSTRAINT FK_zamestnanec_upominka_casopis__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE ctenar_upominka_kniha ADD CONSTRAINT FK_ctenar_upominka_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE ctenar_upominka_kniha ADD CONSTRAINT FK_ctenar_upominka_kniha__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;
ALTER TABLE ctenar_upominka_casopis ADD CONSTRAINT FK_ctenar_upominka_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;
ALTER TABLE ctenar_upominka_casopis ADD CONSTRAINT FK_ctenar_upominka_casopis__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;

ALTER TABLE zamestnanec ADD CONSTRAINT FK_zamestnanec__kontaktni_udaje_ID FOREIGN KEY (kontaktni_udaje_ID) REFERENCES kontaktni_udaje ON DELETE CASCADE;
ALTER TABLE ctenar ADD CONSTRAINT FK_ctenar__kontaktni_udaje_ID FOREIGN KEY (kontaktni_udaje_ID) REFERENCES kontaktni_udaje ON DELETE CASCADE;
ALTER TABLE kniha ADD CONSTRAINT FK_kniha__nakladatelstvi_ID FOREIGN KEY (nakladatelstvi_ID) REFERENCES nakladatelstvi ON DELETE CASCADE;
ALTER TABLE casopis ADD CONSTRAINT FK_casopis__nakladatelstvi_ID FOREIGN KEY (nakladatelstvi_ID) REFERENCES nakladatelstvi ON DELETE CASCADE;

ALTER TABLE autor_napsal_kniha ADD CONSTRAINT FK_autor_napsal_kniha__autor_ID FOREIGN KEY (autor_ID) REFERENCES autor ON DELETE CASCADE;
ALTER TABLE autor_napsal_kniha ADD CONSTRAINT FK_autor_napsal_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE autor_napsal_casopis ADD CONSTRAINT FK_autor_napsal_casopis__autor_ID FOREIGN KEY (autor_ID) REFERENCES autor ON DELETE CASCADE;
ALTER TABLE autor_napsal_casopis ADD CONSTRAINT FK_autor_napsal_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;

ALTER TABLE zamestnanec_rezervace_kniha ADD CONSTRAINT FK_zamestnanec_rezervace_kniha__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE zamestnanec_rezervace_kniha ADD CONSTRAINT FK_zamestnanec_rezervace_kniha_kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE zamestnanec_rezervace_casopis ADD CONSTRAINT FK_zamestnanec_rezervace_casopis__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE zamestnanec_rezervace_casopis ADD CONSTRAINT FK_zamestnanec_rezervace_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;
ALTER TABLE ctenar_rezervace_kniha ADD CONSTRAINT FK_ctenar_rezervace_kniha__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;
ALTER TABLE ctenar_rezervace_kniha ADD CONSTRAINT FK_ctenar_rezervace_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE ctenar_rezervace_casopis ADD CONSTRAINT FK_ctenar_rezervace_casopis__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;
ALTER TABLE ctenar_rezervace_casopis ADD CONSTRAINT FK_ctenar_rezervace_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;

ALTER TABLE zamestnanec_vypujcka_kniha ADD CONSTRAINT FK_zamestnanec_vypujcka_kniha__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE zamestnanec_vypujcka_kniha ADD CONSTRAINT FK_zamestnanec_vypujcka_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE zamestnanec_vypujcka_casopis ADD CONSTRAINT FK_zamestnanec_vypujcka_casopis__zamestnanec_ID FOREIGN KEY (zamestnanec_ID) REFERENCES zamestnanec ON DELETE CASCADE;
ALTER TABLE zamestnanec_vypujcka_casopis ADD CONSTRAINT FK_zamestnanec_vypujcka_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;
ALTER TABLE ctenar_vypujcka_kniha ADD CONSTRAINT FK_ctenar_vypujcka_kniha__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;
ALTER TABLE ctenar_vypujcka_kniha ADD CONSTRAINT FK_ctenar_vypujcka_kniha__kniha_ISBN FOREIGN KEY (kniha_ISBN) REFERENCES kniha ON DELETE CASCADE;
ALTER TABLE ctenar_vypujcka_casopis ADD CONSTRAINT FK_ctenar_vypujcka_casopis__ctenar_ID FOREIGN KEY (ctenar_ID) REFERENCES ctenar ON DELETE CASCADE;
ALTER TABLE ctenar_vypujcka_casopis ADD CONSTRAINT FK_ctenar_vypujcka_casopis__casopis_ISSN_casopis_cislo FOREIGN KEY (casopis_ISSN, casopis_cislo) REFERENCES casopis ON DELETE CASCADE;

-- Dvousloupcová omezení --
ALTER TABLE zamestnanec_vypujcka_kniha ADD CONSTRAINT zamestnanec_vypujcka_kniha__date_constraint CHECK(vypujcka_do_kdy > vypujcka_od_kdy);
ALTER TABLE zamestnanec_vypujcka_casopis ADD CONSTRAINT zamestnanec_vypujcka_casopis__date_constraint CHECK(vypujcka_do_kdy > vypujcka_od_kdy);
ALTER TABLE ctenar_vypujcka_kniha ADD CONSTRAINT ctenar_vypujcka_kniha__date_constraint CHECK(vypujcka_do_kdy > vypujcka_od_kdy);
ALTER TABLE ctenar_vypujcka_casopis ADD CONSTRAINT ctenar_vypujcka_casopis__date_constraint CHECK(vypujcka_do_kdy > vypujcka_od_kdy);

ALTER TABLE ctenar ADD CONSTRAINT ctenar__date_constraint CHECK(ctenar_clen_od < ctenar_clen_do);

------------- Trigery --------------------

-- Autogenerování sekvence ID pro čtenáře
DROP SEQUENCE ID_seq;
CREATE SEQUENCE ID_seq START WITH 1;

CREATE OR REPLACE TRIGGER vytvoreni_ID
BEFORE INSERT ON ctenar
FOR EACH ROW
BEGIN
    SELECT ID_seq.NEXTVAL
    INTO   :new.ctenar_ID
    FROM   dual;
END;
/

-- Odečtení počtu výtisků knihy, když si ji někdo půjčí
CREATE OR REPLACE TRIGGER Pujcka_zmen_pocet_vytisku
    BEFORE INSERT ON ctenar_vypujcka_kniha
        FOR EACH ROW
DECLARE
BEGIN
    sniz_pocet_vytisku(:new.kniha_ISBN);
END;
/

------------- Procedury ---------------

-- Výpočet procentuálního zastoupení knih zadaného žánru
CREATE OR REPLACE PROCEDURE zastoupeni_zanru
(
    zanr IN kniha.kniha_zanr%TYPE
) IS

    celkem_knih   NUMBER;
    knih_zanru      NUMBER;
    CURSOR c_kniha IS SELECT * FROM kniha;
    zaznam kniha%ROWTYPE;

BEGIN
    knih_zanru := 0;
    celkem_knih := 0;

    SELECT COUNT(*) INTO celkem_knih FROM kniha;

    for zaznam in c_kniha loop
        if (zaznam.kniha_zanr = zanr) then
            knih_zanru := knih_zanru + 1;
        end if;

    end loop;

    dbms_output.put_line('Knihovna vlastní ' || knih_zanru / celkem_knih * 100 || '% knih zadaného žánru: ' || zanr);
EXCEPTION
    WHEN zero_divide THEN
        dbms_output.put_line('Knihovna vlastní 0% knih zadaného žánru: ' || zanr);
END;
/

-- Odečtení počtu výtisků knihy, když si ji někdo půjčí
CREATE OR REPLACE PROCEDURE sniz_pocet_vytisku (k_isbn kniha.kniha_ISBN%TYPE) IS
vytisky kniha.kniha_pocet_vytisku%TYPE;
CURSOR c
IS
    SELECT kniha_pocet_vytisku
    FROM kniha
    WHERE k_isbn = kniha.kniha_ISBN;

BEGIN
  OPEN c;
  FETCH c INTO vytisky;

  IF c%found then

    vytisky := vytisky-1;
    UPDATE kniha 
    SET kniha_pocet_vytisku = vytisky
    WHERE k_isbn = kniha.kniha_ISBN;
  END IF;

END;
/

----------------- Naplnění daty ------------------

INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Brno', 45612, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Shelter Island, NY', 11964, 'USA');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Praha 1', 11846, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Modřice', 45687, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Šlapanice', 21612, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Tišnov', 84721, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Brno', 60200, 'CZE');
INSERT INTO mesto ( mesto_nazev, mesto_psc, mesto_stat)
VALUES ( 'Frenštát pod Radhoštěm', 74401, 'CZE');

INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(123, 'Rovná', 1, 420456789123, 'j.novo@seznam.cz');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(146, 'Rovná', 1, 420484589123, 'jakubn@gmail.com');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(20, 'Baldwin Road', 2, 2036261510, 'support@manning.com');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(205, 'Petřín', 3, 420603759280, 'info@astropis.cz');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email)  
VALUES(458, 'Brněnská', 4, 420452111223, 'zacpaln@knihovna.cz');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email)  
VALUES(526, 'Dlouhá', 7, 420756884129, 'milan.kohout@seznam.cz');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(12, 'Křivá', 5, 420667121128, 'dave.zmeskal@gmail.com');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(42, 'Krátká', 6, 420456654121, 'brabec.oscar@protonmail.com');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(5, 'Radlas', 7, 420545212747, 'nakladatelstvi@hostbrno.cz');
INSERT INTO kontaktni_udaje (kontaktni_udaje_cp, kontaktni_udaje_ulice, mesto_ID, kontaktni_udaje_tel, kontaktni_udaje_email) 
VALUES(17, 'Masarykova', 8, 420556831361, 'svoboda@polarissf.cz');


INSERT INTO nakladatelstvi (nakladatelstvi_nazev, kontaktni_udaje_ID)
VALUES('Manning Publications', 3);
INSERT INTO nakladatelstvi (nakladatelstvi_nazev, kontaktni_udaje_ID)
VALUES('Astropis', 4);
INSERT INTO nakladatelstvi (nakladatelstvi_nazev, kontaktni_udaje_ID)
VALUES('Host', 9);
INSERT INTO nakladatelstvi (nakladatelstvi_nazev, kontaktni_udaje_ID)
VALUES('Polaris', 10);

INSERT INTO ctenar (ctenar_jmeno, ctenar_prijmeni, ctenar_clen_od, ctenar_clen_do, kontaktni_udaje_id) 
VALUES('Jan', 'Novotný', TO_DATE( '15-JUN-18', 'DD-MON-RR' ), TO_DATE( '15-JUN-23', 'DD-MON-RR' ), 1);
INSERT INTO ctenar (ctenar_jmeno, ctenar_prijmeni, ctenar_clen_od, ctenar_clen_do, kontaktni_udaje_id) 
VALUES('Milan', 'Kohout', TO_DATE( '20-OCT-19', 'DD-MON-RR' ), TO_DATE( '20-OCT-24', 'DD-MON-RR' ), 6);
INSERT INTO ctenar (ctenar_jmeno, ctenar_prijmeni, ctenar_clen_od, ctenar_clen_do, kontaktni_udaje_id) 
VALUES('David', 'Zmeškal', TO_DATE( '2-FEB-20', 'DD-MON-RR' ), TO_DATE( '2-FEB-25', 'DD-MON-RR' ), 7);
INSERT INTO ctenar (ctenar_jmeno, ctenar_prijmeni, ctenar_clen_od, ctenar_clen_do, kontaktni_udaje_id) 
VALUES('Oskar', 'Brabec', TO_DATE( '5-MAY-18', 'DD-MON-RR' ), TO_DATE( '5-MAY-23', 'DD-MON-RR' ), 8);

INSERT INTO pozice (pozice_nazev, pozice_plat)
VALUES ('Knihovnik', 23000);
INSERT INTO pozice (pozice_nazev, pozice_plat)
VALUES ('Vedoucí', 32000);

INSERT INTO zamestnanec (zamestnanec_jmeno, zamestnanec_prijmeni, pozice_ID, zamestnanec_rodne_cislo, kontaktni_udaje_ID)
VALUES('Jakub', 'Novák', 1, 7810207251, 2);
INSERT INTO zamestnanec (zamestnanec_jmeno, zamestnanec_prijmeni, pozice_ID, zamestnanec_rodne_cislo, kontaktni_udaje_ID)
VALUES('Norbert', 'Zacpal', 2, 7503107271, 5);

INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('John', 'Sonmez', 'Naučná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Soňa', 'Ehlerová', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('David', 'Ondřich', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jan', 'Palouš', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Michal', 'Švanda', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jiří', 'Grygar', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jaroslav', 'Haas', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Daniela', 'Korčáková', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Ivana', 'Orlitová', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Petra', 'Suková', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Tereza', 'Jeřábková', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jana', 'Tichá', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jan', 'Vondrák', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Jiří', 'Borovička', 'Odborná literatura', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Liou', 'Cch-sin', 'sci-fi', '21. století');
INSERT INTO autor (autor_jmeno, autor_prijmeni, autor_zanr, autor_obdobi)
VALUES('Dan', 'Abnett', 'sci-fi', '21. století');

INSERT INTO kniha (kniha_ISBN, kniha_jmeno, kniha_pocet_stran, kniha_pocet_vytisku, kniha_rok_vydani, kniha_zanr, kniha_cislo_vydani, nakladatelstvi_ID)
VALUES('1617292397', 'Soft Skills: The software developers life manual', 504, 4, 2015, 'Naučná literatura', 1, 1);
INSERT INTO kniha (kniha_ISBN, kniha_jmeno, kniha_pocet_stran, kniha_pocet_vytisku, kniha_rok_vydani, kniha_zanr, kniha_cislo_vydani, nakladatelstvi_ID)
VALUES('9788027547', 'Kulový blesk', 416, 5, 2019, 'sci-fi', 1, 3);
INSERT INTO kniha (kniha_ISBN, kniha_jmeno, kniha_pocet_stran, kniha_pocet_vytisku, kniha_rok_vydani, kniha_zanr, kniha_cislo_vydani, nakladatelstvi_ID)
VALUES('9788073328', 'Warhammer 40k: Stvořitel duchů', 360, 6, 2007, 'sci-fi', 1, 4);

INSERT INTO casopis (casopis_ISSN, casopis_jmeno, casopis_pocet_stran, casopis_pocet_vytisku, casopis_rok_vydani, casopis_cislo, casopis_rocnik, casopis_tema, nakladatelstvi_ID)
VALUES('1211048519', 'ASTROPIS S/2019', 59, 3, 2019, 5, 2019, 'astronomie', 2);

INSERT INTO autor_napsal_kniha 
VALUES(1, '1617292397');
INSERT INTO autor_napsal_kniha 
VALUES(15, '9788027547');
INSERT INTO autor_napsal_kniha 
VALUES(16, '9788073328');
INSERT INTO autor_napsal_casopis
VALUES(2, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(3, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(4, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(5, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(6, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(7, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(8, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(9, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(10, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(11, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(12, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(13, '1211048519', 5);
INSERT INTO autor_napsal_casopis
VALUES(14, '1211048519', 5);

INSERT INTO zamestnanec_rezervace_kniha
VALUES(TO_DATE( '10-MAY-20', 'DD-MON-RR' ), 1, '9788027547');

INSERT INTO ctenar_rezervace_kniha 
VALUES(TO_DATE( '25-APR-20', 'DD-MON-RR' ), 2, '1617292397');
INSERT INTO ctenar_rezervace_kniha 
VALUES(TO_DATE( '11-APR-20', 'DD-MON-RR' ), 4, '1617292397');
INSERT INTO ctenar_rezervace_kniha 
VALUES(TO_DATE( '11-APR-20', 'DD-MON-RR' ), 4, '9788027547');

INSERT INTO zamestnanec_rezervace_casopis
VALUES(TO_DATE('15-APR-20', 'DD-MON-RR' ), 2, '1211048519', 5);

INSERT INTO ctenar_rezervace_casopis
VALUES(TO_DATE('3-MAY-20', 'DD-MON-RR' ), 3, '1211048519', 5);

INSERT INTO zamestnanec_vypujcka_kniha
VALUES(TO_DATE( '10-FEB-20', 'DD-MON-RR' ), TO_DATE( '10-MAY-20', 'DD-MON-RR' ), 2, '9788027547');

INSERT INTO zamestnanec_vypujcka_casopis
VALUES(TO_DATE( '29-JAN-20', 'DD-MON-RR' ), TO_DATE( '29-APR-20', 'DD-MON-RR' ), 1, '1211048519', 5);

INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '16-DEC-19', 'DD-MON-RR' ), TO_DATE( '16-FEB-20', 'DD-MON-RR' ), 1, '9788027547');
INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '5-SEP-19', 'DD-MON-RR' ), TO_DATE( '5-DEC-19', 'DD-MON-RR' ), 4, '1617292397');
INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '5-SEP-19', 'DD-MON-RR' ), TO_DATE( '5-DEC-19', 'DD-MON-RR' ), 4, '9788073328');
INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '20-OCT-19', 'DD-MON-RR' ), TO_DATE( '20-JAN-20', 'DD-MON-RR' ), 2, '9788073328');
INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '21-FEB-20', 'DD-MON-RR' ), TO_DATE( '21-MAY-20', 'DD-MON-RR' ), 3, '9788073328');

INSERT INTO ctenar_vypujcka_casopis
VALUES(TO_DATE( '16-JAN-20', 'DD-MON-RR' ), TO_DATE( '16-APR-20', 'DD-MON-RR' ), 1, '1211048519', 5);

INSERT INTO ctenar_upominka_kniha (upominka_vystaveno, ctenar_ID, kniha_ISBN)
VALUES(TO_DATE( '17-FEB-20', 'DD-MON-RR' ), 1, '9788027547');
INSERT INTO ctenar_upominka_kniha (upominka_vystaveno, ctenar_ID, kniha_ISBN)
VALUES(TO_DATE( '17-FEB-20', 'DD-MON-RR' ), 1, '1617292397');
INSERT INTO zamestnanec_upominka_kniha (upominka_vystaveno, zamestnanec_ID, kniha_ISBN)
VALUES(TO_DATE( '17-FEB-20', 'DD-MON-RR' ), 1, '1617292397');

INSERT INTO ctenar_upominka_kniha (upominka_vystaveno, ctenar_ID, kniha_ISBN)
VALUES(TO_DATE( '17-FEB-20', 'DD-MON-RR' ), 2, '1617292397');
INSERT INTO ctenar_upominka_casopis (upominka_vystaveno, ctenar_ID, casopis_ISSN, casopis_cislo)
VALUES(TO_DATE( '17-FEB-20', 'DD-MON-RR' ), 2, '1211048519', 5);

------------------------- Proj část 3 --------------------

-- Zobrazení kontaktních údajů na uživatele
SELECT ctenar_jmeno, ctenar_prijmeni, kontaktni_udaje_tel, kontaktni_udaje_email 
FROM ctenar INNER JOIN kontaktni_udaje ON ctenar.kontaktni_udaje_ID = kontaktni_udaje.kontaktni_udaje_ID;

-- Jaké knihy jsou od nakladatelství Polaris
SELECT kniha_jmeno
FROM kniha
INNER JOIN nakladatelstvi
ON kniha.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
WHERE nakladatelstvi_nazev = 'Polaris';

-- Které knihy napsal autor Liou Cch-sin
SELECT kniha_jmeno FROM kniha, autor, autor_napsal_kniha  WHERE kniha.kniha_ISBN = autor_napsal_kniha.kniha_ISBN
                                                                AND autor.autor_ID = autor_napsal_kniha.autor_ID 
                                                                AND autor.autor_jmeno = 'Liou' 
                                                                AND autor.autor_prijmeni = 'Cch-sin';

-- Kontakt na nakladatelství, které vydává časopis ASTROPIS S/2019
SELECT nakladatelstvi_nazev, kontaktni_udaje_tel, kontaktni_udaje_email FROM nakladatelstvi, kontaktni_udaje, casopis WHERE casopis.casopis_jmeno = 'ASTROPIS S/2019'
                                                                                                                            AND casopis.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
                                                                                                                            AND nakladatelstvi.kontaktni_udaje_ID = kontaktni_udaje.kontaktni_udaje_ID;

-- Jaké knihy mají uživatelé vypůjčené
SELECT ctenar_jmeno AS "jmeno", ctenar_prijmeni AS "prijmeni", kniha_jmeno 
FROM ctenar_vypujcka_kniha 
INNER JOIN kniha
ON kniha.kniha_ISBN = ctenar_vypujcka_kniha.kniha_ISBN
INNER JOIN ctenar 
ON ctenar.ctenar_ID = ctenar_vypujcka_kniha.ctenar_ID
UNION
SELECT zamestnanec_jmeno AS "jmeno", zamestnanec_prijmeni AS "prijmeni", kniha_jmeno 
FROM zamestnanec_vypujcka_kniha 
INNER JOIN kniha
ON zamestnanec_vypujcka_kniha.kniha_ISBN = kniha.kniha_ISBN
INNER JOIN zamestnanec
ON zamestnanec.zamestnanec_ID = zamestnanec_vypujcka_kniha.zamestnanec_ID;

-- Vypsání na jaké knihy má který uživatel upomínky
SELECT kniha_jmeno, ctenar_jmeno AS "jmeno", ctenar_prijmeni AS "prijmeni", upominka_vystaveno
FROM ctenar_upominka_kniha
INNER JOIN kniha
ON kniha.kniha_ISBN = ctenar_upominka_kniha.kniha_ISBN
INNER JOIN ctenar
ON ctenar.ctenar_ID = ctenar_upominka_kniha.ctenar_ID
UNION
SELECT kniha_jmeno, zamestnanec_jmeno AS "jmeno", zamestnanec_prijmeni AS "prijmeni", upominka_vystaveno
FROM zamestnanec_upominka_kniha
INNER JOIN kniha
ON kniha.kniha_ISBN = zamestnanec_upominka_kniha.kniha_ISBN
INNER JOIN zamestnanec
ON zamestnanec.zamestnanec_ID = zamestnanec_upominka_kniha.zamestnanec_ID;

-- Kolik upomínek má který uživatel
SELECT jmeno, prijmeni, SUM(upominky) FROM(
    SELECT ctenar_jmeno AS jmeno, ctenar_prijmeni AS prijmeni, kniha_jmeno, COUNT(*) AS upominky
    FROM ctenar_upominka_kniha
    INNER JOIN kniha
    ON kniha.kniha_ISBN = ctenar_upominka_kniha.kniha_ISBN
    INNER JOIN ctenar
    ON ctenar.ctenar_ID = ctenar_upominka_kniha.ctenar_ID
    GROUP BY ctenar_jmeno, ctenar_prijmeni, kniha_jmeno
    UNION
    SELECT zamestnanec_jmeno AS jmeno, zamestnanec_prijmeni AS prijmeni, kniha_jmeno, COUNT(*) AS upominky
    FROM zamestnanec_upominka_kniha
    INNER JOIN kniha
    ON kniha.kniha_ISBN = zamestnanec_upominka_kniha.kniha_ISBN
    INNER JOIN zamestnanec
    ON zamestnanec.zamestnanec_ID = zamestnanec_upominka_kniha.zamestnanec_ID
    GROUP BY zamestnanec_jmeno, zamestnanec_prijmeni, kniha_jmeno
    UNION
    SELECT ctenar_jmeno AS jmeno, ctenar_prijmeni AS prijmeni, casopis_jmeno, COUNT(*) AS upominky 
    FROM ctenar_upominka_casopis
    INNER JOIN casopis
    ON casopis.casopis_ISSN = ctenar_upominka_casopis.casopis_ISSN
    INNER JOIN ctenar
    ON ctenar.ctenar_ID = ctenar_upominka_casopis.ctenar_ID
    GROUP BY ctenar_jmeno, ctenar_prijmeni, casopis_jmeno
    UNION
    SELECT zamestnanec_jmeno AS jmeno, zamestnanec_prijmeni AS prijmeni, casopis_jmeno, COUNT(*) AS upominky 
    FROM zamestnanec_upominka_casopis
    INNER JOIN casopis
    ON casopis.casopis_ISSN = zamestnanec_upominka_casopis.casopis_ISSN
    INNER JOIN zamestnanec
    ON zamestnanec.zamestnanec_ID = zamestnanec_upominka_casopis.zamestnanec_ID
    GROUP BY zamestnanec_jmeno, zamestnanec_prijmeni, casopis_jmeno
) GROUP BY jmeno, prijmeni;    

-- Kolik knih je kterého žánru
SELECT kniha_zanr, COUNT(kniha_ISBN) AS pocet_knih FROM kniha GROUP BY kniha_zanr;

-- Průměrný počet sránek knih podle žánru
SELECT kniha_zanr, AVG(kniha_pocet_stran) AS prumerny_pocet_stránek FROM kniha GROUP BY kniha_zanr;

-- Kdo má rezervované které knihy
SELECT ctenar_jmeno AS jmeno, ctenar_prijmeni AS primeni, kniha_jmeno FROM kniha INNER JOIN ctenar_rezervace_kniha ON kniha.kniha_ISBN = ctenar_rezervace_kniha.kniha_ISBN 
                                                                                 INNER JOIN ctenar ON ctenar_rezervace_kniha.ctenar_ID = ctenar.ctenar_ID  
    WHERE EXISTS(SELECT * FROM ctenar_rezervace_kniha, zamestnanec_rezervace_kniha WHERE ctenar_rezervace_kniha.kniha_ISBN = kniha.kniha_ISBN OR zamestnanec_rezervace_kniha.kniha_ISBN = kniha.kniha_ISBN)
UNION
SELECT zamestnanec_jmeno AS jmeno, zamestnanec_prijmeni AS primeni, kniha_jmeno FROM kniha INNER JOIN zamestnanec_rezervace_kniha ON kniha.kniha_ISBN = zamestnanec_rezervace_kniha.kniha_ISBN 
                                                                                           INNER JOIN zamestnanec ON zamestnanec_rezervace_kniha.zamestnanec_ID = zamestnanec.zamestnanec_ID  
    WHERE EXISTS(SELECT * FROM ctenar_rezervace_kniha, zamestnanec_rezervace_kniha WHERE ctenar_rezervace_kniha.kniha_ISBN = kniha.kniha_ISBN OR zamestnanec_rezervace_kniha.kniha_ISBN = kniha.kniha_ISBN);

-- Které nakladatelstvý vydávají knihy žánru "sci-fi"
SELECT nakladatelstvi_nazev FROM nakladatelstvi WHERE nakladatelstvi.nakladatelstvi_ID IN (SELECT kniha.nakladatelstvi_ID FROM kniha WHERE kniha_zanr = 'sci-fi');


------------------------- Proj část 4 --------------------
-- Vymazání změn
DROP VIEW polaris_books_xkuder04;
DROP MATERIALIZED VIEW polaris_books_materialized_xkuder04;
DROP VIEW polaris_books_xhorak71;
DROP MATERIALIZED VIEW polaris_books_materialized_xhorak71;

DROP INDEX nakladatelstvi_index;
DROP INDEX kniha_index;

-- Definice přístupovách práv pro uživatele xkuder04
GRANT ALL ON kontaktni_udaje TO xkuder04;
GRANT ALL ON mesto TO xkuder04;
GRANT ALL ON nakladatelstvi TO xkuder04;
GRANT ALL ON zamestnanec_upominka_kniha TO xkuder04;
GRANT ALL ON zamestnanec_upominka_casopis TO xkuder04;
GRANT ALL ON ctenar_upominka_kniha TO xkuder04;
GRANT ALL ON ctenar_upominka_casopis TO xkuder04;
GRANT ALL ON zamestnanec TO xkuder04;
GRANT ALL ON pozice TO xkuder04;
GRANT ALL ON ctenar TO xkuder04;
GRANT ALL ON autor TO xkuder04;
GRANT ALL ON kniha TO xkuder04;
GRANT ALL ON casopis TO xkuder04;
GRANT ALL ON autor_napsal_kniha TO xkuder04;
GRANT ALL ON autor_napsal_casopis TO xkuder04;
GRANT ALL ON zamestnanec_rezervace_kniha TO xkuder04;
GRANT ALL ON zamestnanec_rezervace_casopis TO xkuder04;
GRANT ALL ON ctenar_rezervace_kniha TO xkuder04;
GRANT ALL ON ctenar_rezervace_casopis TO xkuder04;
GRANT ALL ON zamestnanec_vypujcka_kniha TO xkuder04;
GRANT ALL ON zamestnanec_vypujcka_casopis TO xkuder04;
GRANT ALL ON ctenar_vypujcka_kniha TO xkuder04;
GRANT ALL ON ctenar_vypujcka_casopis TO xkuder04;

-- Definice přístupových práv pro uživatele xhorak71
GRANT ALL ON kontaktni_udaje TO xhorak71;
GRANT ALL ON mesto TO xhorak71;
GRANT ALL ON nakladatelstvi TO xhorak71;
GRANT ALL ON zamestnanec_upominka_kniha TO xhorak71;
GRANT ALL ON zamestnanec_upominka_casopis TO xhorak71;
GRANT ALL ON ctenar_upominka_kniha TO xhorak71;
GRANT ALL ON ctenar_upominka_casopis TO xhorak71;
GRANT ALL ON zamestnanec TO xhorak71;
GRANT ALL ON pozice TO xhorak71;
GRANT ALL ON ctenar TO xhorak71;
GRANT ALL ON autor TO xhorak71;
GRANT ALL ON kniha TO xhorak71;
GRANT ALL ON casopis TO xhorak71;
GRANT ALL ON autor_napsal_kniha TO xhorak71;
GRANT ALL ON autor_napsal_casopis TO xhorak71;
GRANT ALL ON zamestnanec_rezervace_kniha TO xhorak71;
GRANT ALL ON zamestnanec_rezervace_casopis TO xhorak71;
GRANT ALL ON ctenar_rezervace_kniha TO xhorak71;
GRANT ALL ON ctenar_rezervace_casopis TO xhorak71;
GRANT ALL ON zamestnanec_vypujcka_kniha TO xhorak71;
GRANT ALL ON zamestnanec_vypujcka_casopis TO xhorak71;
GRANT ALL ON ctenar_vypujcka_kniha TO xhorak71;
GRANT ALL ON ctenar_vypujcka_casopis TO xhorak71;

----- Materializovaný pohled s ukázkou funkčnosti ------

-- Využití tabulek xhorak71
-- Obyčejný pohled
CREATE VIEW polaris_books_xhorak71 AS
    SELECT kniha_jmeno
    FROM xhorak71.kniha
    INNER JOIN xhorak71.nakladatelstvi
    ON kniha.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
    WHERE nakladatelstvi_nazev = 'Polaris';

-- Materializovaný pohled
CREATE MATERIALIZED VIEW polaris_books_materialized_xhorak71 AS
    SELECT kniha_jmeno
    FROM xhorak71.kniha
    INNER JOIN xhorak71.nakladatelstvi
    ON kniha.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
    WHERE nakladatelstvi_nazev = 'Polaris';

-- Insert 
INSERT INTO xhorak71.kniha (kniha_ISBN, kniha_jmeno, kniha_pocet_stran, kniha_pocet_vytisku, kniha_rok_vydani, kniha_zanr, kniha_cislo_vydani, nakladatelstvi_ID)
VALUES('9788073301', 'Warhammer 40k: Let Eisensteinu', 334, 5, 2018, 'sci-fi', 1, 4);

-- Využití tabulek xkuder04
-- Obyčejný pohled
CREATE VIEW polaris_books_xkuder04 AS
    SELECT kniha_jmeno
    FROM xkuder04.kniha
    INNER JOIN xkuder04.nakladatelstvi
    ON kniha.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
    WHERE nakladatelstvi_nazev = 'Polaris';

-- Materializovaný pohled
CREATE MATERIALIZED VIEW polaris_books_materialized_xkuder04 AS
    SELECT kniha_jmeno
    FROM xkuder04.kniha
    INNER JOIN xkuder04.nakladatelstvi
    ON kniha.nakladatelstvi_ID = nakladatelstvi.nakladatelstvi_ID
    WHERE nakladatelstvi_nazev = 'Polaris';

-- Insert 
INSERT INTO xkuder04.kniha (kniha_ISBN, kniha_jmeno, kniha_pocet_stran, kniha_pocet_vytisku, kniha_rok_vydani, kniha_zanr, kniha_cislo_vydani, nakladatelstvi_ID)
VALUES('9788073301', 'Warhammer 40k: Let Eisensteinu', 334, 5, 2018, 'sci-fi', 1, 4);

-- Demonstrace
SELECT * FROM polaris_books_xhorak71; -- Obsahuje novou položku
SELECT * FROM polaris_books_materialized_xhorak71; -- Toto ne

SELECT * FROM polaris_books_xkuder04; -- Obsahuje novou položku
SELECT * FROM polaris_books_materialized_xkuder04; -- Toto ne

------------------ Test procedur a trigerů -------------------

-- Test procedury
-- Výpis kolik procent ze všech knih je knih zadaného žánru
EXEC zastoupeni_zanru('sci-fi');
EXEC zastoupeni_zanru('test');


-- Test trigru spojeného s procedurou
-- Odečtení počtu výtisků u knihy, když jsi ji někdo půjčí
SELECT kniha_pocet_vytisku FROM kniha WHERE kniha_ISBN = '9788027547';

INSERT INTO ctenar_vypujcka_kniha
VALUES(TO_DATE( '15-DEC-19', 'DD-MON-RR' ), TO_DATE( '15-FEB-20', 'DD-MON-RR' ), 2, '9788027547');

SELECT kniha_pocet_vytisku FROM kniha WHERE kniha_ISBN = '9788027547';

------------------------ Vytvoření indexu ----------------------------------------

-- Použití explain plan na tabelce bez indexu
EXPLAIN PLAN FOR
SELECT nakladatelstvi_nazev, COUNT(kniha_ISBN) AS pocet_knih FROM nakladatelstvi LEFT OUTER JOIN kniha ON nakladatelstvi.nakladatelstvi_ID = kniha.nakladatelstvi_ID GROUP BY nakladatelstvi_nazev;

SELECT * FROM TABLE(dbms_xplan.display);

-- Vytvoření indexů které tento select vylepší
--CREATE INDEX nakladatelstvi_index ON nakladatelstvi(nakladatelstvi_nazev);
CREATE INDEX kniha_index ON kniha(nakladatelstvi_ID);

-- Použít explain plan na vylepšené tabulce
EXPLAIN PLAN FOR
SELECT nakladatelstvi_nazev, COUNT(kniha_ISBN) AS pocet_knih FROM nakladatelstvi LEFT OUTER JOIN kniha ON nakladatelstvi.nakladatelstvi_ID = kniha.nakladatelstvi_ID GROUP BY nakladatelstvi_nazev;

SELECT * FROM TABLE(dbms_xplan.display);